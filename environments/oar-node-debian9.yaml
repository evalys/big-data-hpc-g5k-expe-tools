#==========================================================================
#
# DESCRIPTION: HPC experiments (node) on a grid'5000 Debian 9 NFS image
#
#==========================================================================

---
# extend: grid5000/kameleon-g5k/virtualbox/debian9-x64-nfs.yaml
extend: default/qemu/from_tarball.yaml


global:

  rootfs_archive_url: file:///grid5000/images/debian9-x64-big-2018022209.tgz
  distrib: debian
  release: 9

  # only export in tar Gzipped format
  appliance_formats: tar.gz
  # disable zerofree (no necessary for tar.gz format)
  zerofree: false

  #java_version: openjdk-7-jre-headless
  install_dir: /opt

  author_full_name: Olivier Richard
  g5k_user: orichard
  g5k_images_path: /home/$${g5k_user}/my_g5k_images
  g5k_image_version: 1

bootstrap:
  - "@base"

setup:
  - configure_tz:
    - timezone: Europe/Paris
    - set_timezone:
      - exec_in: echo "$${timezone}" > /etc/timezone
      - exec_in: "dpkg-reconfigure -f noninteractive tzdata 2>&1"
  - config_common:
    - ssh:
      - exec_in: echo "Enable non interactive SSH"
      - append_in:
        - /etc/ssh/ssh_config
        - |
          Host *
              ForwardX11 no
              StrictHostKeyChecking no
              PasswordAuthentication no
              AddressFamily inet
    - bash_is_default_sh:
      - exec_in: |
          # make /bin/sh symlink to bash instead of dash:
          echo "dash dash/sh boolean false" | debconf-set-selections
          DEBIAN_FRONTEND=noninteractive dpkg-reconfigure dash
    - install_experiment_mandatory:
      - exec_in: apt install -y sudo

  # External steps
  # Big data tools
  #- java
  #- hadoop
  #- spark
  #- flink
  #- hbase
  #- cassandra

  # HPC tools
  - oar-node
  # Only one HPC resource manager is needed!
  #- slurm-node
  #- NasParallelBenchmark


export:
  - "@base"
  - create_kaenv:
    - create_env_file:
      - write_local:
        - $${g5k_images_path}/$${kameleon_recipe_name}_$${kameleon_short_uuid}.yaml
        - |
            ---
            name: $${kameleon_recipe_name}
            version: $${g5k_image_version}
            description: Bebida project image uuid=$${kameleon_short_uuid}
            author: $${author_full_name}
            visibility: shared
            destructive: false
            os: linux
            image:
              file: $${g5k_images_path}/$${kameleon_recipe_name}_$${kameleon_short_uuid}.tar.gz
              kind: tar
              compression: gzip
            postinstalls:
            - archive: server:///grid5000/postinstalls/g5k-postinstall.tgz
              compression: gzip
              script: g5k-postinstall --net debian --net traditional-names --net hpc --fstab nfs --fstab site-specific
            boot:
              kernel: "/vmlinuz"
              initrd: "/initrd.img"
              kernel_params: net.ifnames=0 biosdevname=0 modprobe.blacklist=myri10ge
            filesystem: ext4
            partition_type: 131
            multipart: false
    - copy_applicance:
      - exec_local: cp -av $${appliance_filename}.tar.gz $${g5k_images_path}/$${kameleon_recipe_name}_$${kameleon_short_uuid}.tar.gz
      - exec_local: |
          echo -e "To import the environment to Kadeploy:\nkaenv3 -a $${g5k_images_path}/$${kameleon_recipe_name}_$${kameleon_short_uuid}.yaml"
      - exec_local: echo -e "To backup the build cache in the same environment:\n cp -av $${kameleon_cwd}/$${kameleon_recipe_name}-cache.tar.gz $${g5k_images_path}/$${kameleon_recipe_name}-cache_$${kameleon_short_uuid}.tar.gz"
